
var insertName = function(n) {
    var names = [
        "Kötturinn Njáll",
        "Glæpamaðurinn",
    ]
    return names[n]
}

var insertImage = function(n) {
    var images = [
        "http://www.newrivercabinetry.com/wp-content/uploads/2018/12/all-about-tuxedo-cats-facts-lifespan-and-intelligence-petite-black-white-cat-valuable-4.jpg",
        "http://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwik2Pyhq_DgAhWux4UKHRM6CmgQjRx6BAgBEAU&url=http%3A%2F%2Fcriminaljusticeschoolinfo.com%2Flegal-justice-news%2F2014%2F05%2Fcan-i-become-a-cop-with-a-criminal-record-22514%2F&psig=AOvVaw1olGIucLxGAD-dz4iQj5qs&ust=1552058671760417"
    ]
    return images[n] 
}

var makeDiv = function(n) {
    if(!insertName(n)) {
        return false
    }
    var div = `
        <h2>${insertName(n)}</h2>
        <img src="${insertImage(n)}">
    `
    return div; 
}

var renderCharacter = function(n) {
    if(insertName(n)) {
        document.querySelector("#characters").innerHTML = makeDiv(n);
    }
}

renderCharacter(0); 

module.exports = {
    insertName,
    insertImage,
    makeDiv,
    renderCharacter,
}

